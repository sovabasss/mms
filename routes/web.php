<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true, 'register' => false]);
Route::get('/', 'HomeController@welcome')->name('welcome');
Route::middleware(['auth'])->prefix('admin')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('practices', 'PracticeController');
    Route::resource('employees', 'EmployeeController');
    Route::resource('fop', 'FieldsOfPracticeController');
});
