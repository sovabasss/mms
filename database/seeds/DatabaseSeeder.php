<?php

use App\Models\Employee;
use App\Models\FieldsOfPractice;
use App\Models\Practice;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        factory(FieldsOfPractice::class, 20)->create();
        factory(Practice::class, 10)->create()->each(function ($practice) {
            $practice->employees()->saveMany(factory(Employee::class, rand(3, 8))->make());
            $practice->tags()->sync(FieldsOfPractice::all()->random(4)->pluck('id'));
        });
    }
}
