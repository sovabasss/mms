<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFopPractice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fop_practice', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('practice_id');
            $table->foreign('practice_id')->references('id')->on('practices')->onDelete('cascade');
            $table->unsignedInteger('fop_id');
            $table->foreign('fop_id')->references('id')->on('fields_of_practices')->onDelete('cascade');
            $table->unique(['practice_id', 'fop_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fop_practice');
    }
}
