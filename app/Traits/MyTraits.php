<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

trait MyTraits
{

    public function storeLogo($request)
    {
        return ($request->has('logo')) ? $this->updateLogo($request) : 'storage/logo.png';
    }

    public function updateLogo($request)
    {
        $request->validate([
            'logo' =>  'dimensions:min_width=100,min_height=100',
        ]);
        $path = $request->file('logo')->storeAs('public', Carbon::now()->timestamp . '.' . $request->file('logo')->getClientOriginalExtension());
        return  Storage::url($path);
    }
}
