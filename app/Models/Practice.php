<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\FieldsOfPractice;
use App\Models\Employee;

class Practice extends Model
{
    protected $guarded = [];


    public function tags()
    {
        return $this->belongsToMany(FieldsOfPractice::class, 'fop_practice', 'practice_id', 'fop_id')->withTimestamps();
    }

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    public function getAvatarAttribute()
    {
        return url($this->logo);
    }

    public function getFieldsAttribute()
    {
        return implode($this->tags->pluck('tag')->toArray(), ', ');
    }
}
