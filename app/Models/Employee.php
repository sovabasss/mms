<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Practice;

class Employee extends Model
{
    protected $guarded = [];

    public function practice()
    {
        return $this->belongsTo(Practice::class);
    }
}
