<?php

namespace App\Http\Controllers;

use App\Models\FieldsOfPractice;
use Illuminate\Http\Request;

class FieldsOfPracticeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fop = FieldsOfPractice::with('practices')->orderBy('id', 'desc')->paginate(10);
        return view(request()->route()->getName())->with('fops', $fop);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view(request()->route()->getName());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        FieldsOfPractice::create($request->all());
        return redirect()->route('fop.index')->with('success', 'Stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FieldsOfPractice  $fieldsOfPractice
     * @return \Illuminate\Http\Response
     */
    public function show(FieldsOfPractice $fop)
    {

        return view(request()->route()->getName())->with('fop', $fop);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FieldsOfPractice  $fieldsOfPractice
     * @return \Illuminate\Http\Response
     */
    public function edit(FieldsOfPractice $fop)
    {

        return view(request()->route()->getName())->with('fop', $fop);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FieldsOfPractice  $fieldsOfPractice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FieldsOfPractice $fop)
    {
        $fop->update($request->all());
        return redirect()->route('fop.index')->with('success', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FieldsOfPractice  $fieldsOfPractice
     * @return \Illuminate\Http\Response
     */
    public function destroy(FieldsOfPractice $fop)
    {
        $fop->delete();
        return redirect()->route('fop.index')->with('success', 'Deleted successfully');
    }
}
