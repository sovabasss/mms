<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Blade;
use Faker;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $faker = Faker\Factory::create();

        /**Share menu data to all views */
        $menu = array(
            array('title' => 'Practices', 'route' => 'practices.index', 'desc' => $faker->sentence),
            array('title' => 'Employees', 'route' => 'employees.index', 'desc' => $faker->sentence),
            array('title' => 'Field of Practice', 'route' => 'fop.index', 'desc' => $faker->sentence)
        );
        view()->share('mymenu', $menu);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
