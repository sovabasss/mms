@extends('layouts.app')
@section('content')
<div class="container">
     @component('components.forms',['type'=>'Create new employee'])
@slot('form')
        <form  method="POST" action="{{route('employees.store')}}" enctype="multipart/form-data">         
            <div class="form-group">
                {{csrf_field()}}
                <label for="name">Practice</label>
                <select name="practice_id" class="form-control">
                    @foreach($practices as $practice)                 
                    <option value="{{$practice->id}}">{{$practice->name}}</option>      
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="name">First Name</label>
                <input type="text" class="form-control" placeholder="Enter first name"
                   required name="first_name">
            </div>
            <div class="form-group">
                <label for="text">Last name</label>
                <input type="text" class="form-control" placeholder="Enter last name"
                    required name="last_name">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" placeholder="Enter email" 
                    name="email">
            </div>
            <div class="form-group">
                <label for="email">Phone</label>
                <input type="text" class="form-control" placeholder="Enter phone" 
                    name="phone">
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
           
        </form>
   @endslot
    @endcomponent
</div>
@endsection