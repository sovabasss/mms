@extends('layouts.app')

@section('content')
<div class="container">
  @component('components.notification') @endcomponent
     @component('components.table',['route'=>'employees.create','type'=>'Employees view'])
     @slot('table')
       @if(sizeof($employees)>0)
     <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Practices</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                  
                    <th scope="col">Edit</th>
                  
                    
                </tr>
            </thead>
            <tbody>
              
                @foreach($employees as $employee)
                <tr class="">  
                    <th scope="row">{{$employee->id}}</th>
                    <td><a href="{{route('practices.show', $employee->practice->id)}}">{{$employee->practice->name}}</a></a></td>
                    <td><a href="{{route('employees.show', $employee->id)}}">{{$employee->first_name}}</a></td>
                    <td><a href="{{route('employees.show', $employee->id)}}">{{$employee->last_name}}</a></td>
                    <td>{{$employee->email}}</td>
                    <td>{{$employee->phone}}</td>
                    <td><a href="{{route('employees.edit',['id'=>$employee->id])}}" class="btn btn-sm btn-primary">Edit</a></td>
                    
                </tr>
                @endforeach
            </tbody>
        </table>
      
        @else
        <div class="col-md-12 alert alert-danger text-center">No records</div>
        @endif
         @endslot
         @slot('pagination')
    {{ $employees->links() }}
    @endslot
    @endcomponent
   
</div>
@endsection
  