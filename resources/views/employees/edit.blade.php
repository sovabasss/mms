@extends('layouts.app')
@section('content')
<div class="container">
    @component('components.forms',['type'=>'Edit employee'])
    @slot('form')
    <form method="POST" action="{{route('employees.update',$employee->id)}}" enctype="multipart/form-data">
        {{csrf_field()}}
        @method('PUT')
        <div class="form-group">
            <label for="name">Practice</label>
            <select name="practice_id" class="form-control">
                @foreach($practices as $practice)
                @if($practice->id==$employee->practice->id)
                <option value="{{$practice->id}}" selected="true">{{$practice->name}}</option>
                @else
                <option value="{{$practice->id}}">{{$practice->name}}</option>
                @endif
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="name">First Name</label>
            <input type="text" class="form-control" placeholder="Enter first name" value="{{ $employee->first_name }}"
                required name="first_name">
        </div>
        <div class="form-group">
            <label for="email">Last name</label>
            <input type="email" class="form-control" placeholder="Enter last name" value="{{ $employee->email }}"
                required name="last_name">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" placeholder="Enter email" value="{{ $employee->email }}"
                name="email">
        </div>
        <div class="form-group">
            <label for="email">Phone</label>
            <input type="text" class="form-control" placeholder="Enter phone" value="{{ $employee->phone }}"
                name="phone">
        </div>
        <div class="form-group text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>

    </form>
    @endslot
    @endcomponent
</div>
@endsection