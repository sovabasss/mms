@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-end">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title mb-4">
                        <div class="d-flex justify-content-between">
                           
                            <div class="userData ml-3">
                                <h2 class="d-block" style="font-size: 1.5rem; font-weight: bold"><a
                                        href="javascript:void(0);">{{$employee->first_name}} {{$employee->last_name}}</a></h2>
                                        <a href="mailto:{{$employee->email}}">{{$employee->email}}</a>
                                <h6 class="d-block">
                                <a href="tel:+{{$employee->phone}}">{{$employee->phone}}</a>    
                                </h6>
                                <h6 class="d-block"> <a href="{{route('practices.show',$employee->practice->id)}}" >{{$employee->practice->name}}</a> </h6>
                            </div>
                            <div>
                                <a href="{{route('employees.index')}}" class="btn btn-success">All</a>
                                <a href="{{route('employees.edit',$employee->id)}}" class="btn btn-primary">Edit</a>
                                <a href="{{route('employees.destroy',$employee->id)}}" class="btn btn-danger">Remove</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
