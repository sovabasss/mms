@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row d-flex justify-content-center">
        @foreach($mymenu as $menu)
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">{{$menu['title']}}</div>
                <div class="card-body">
               <h5>{{$menu['desc']}}</h5>
                     <a href="{{route($menu['route'])}}" class="btn btn-success">View more</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
