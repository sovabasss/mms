@extends('layouts.app')
@section('content')
<div class="container">
    @component('components.notification') @endcomponent
    @component('components.table',['route'=>'fop.create','type'=>'Fields of Practice view'])
    @slot('table')
    @if(sizeof($fops)>0)
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Tag</th>

                <th scope="col">Edit</th>
            </tr>
        </thead>
        <tbody>

            @foreach($fops as $fop)
            <tr class="">
                <th scope="row">{{$fop->id}}</th>
                <td><a class="text-capitalize" href="{{route('fop.show',$fop->id)}}">{{$fop->tag}}</a></td>
                <td><a href="{{route('fop.edit',['id'=>$fop->id])}}" class="btn btn-sm btn-primary">Edit</a></td>

            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <div class="col-md-12 alert alert-danger text-center">No records</div>
    @endif
    @endslot
    @slot('pagination')
    {{ $fops->links() }}
    @endslot
    @endcomponent
</div>
@endsection