@extends('layouts.app')
@section('content')
<div class="container">
      @component('components.forms',['type'=>'Update field of practice'])
    @slot('form')
        <form  method="POST" action="{{route('fop.update',$fop->id)}}"
            enctype="multipart/form-data">
            {{csrf_field()}}
            @method('PUT')
            <div class="form-group">
                <label for="name">Tag</label>
                <input type="text" class="form-control"  placeholder="Enter tag"
                    value="{{ $fop->tag }}" required name="tag">
            </div>
            <div class="form-group text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
   @endslot
    @endcomponent
</div>
@endsection