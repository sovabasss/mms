@extends('layouts.app')
@section('content')
<div class="container">
    @component('components.forms',['type'=>'Create new fields of practice'])
    @slot('form')
    <form method="POST" action="{{route('fop.store')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">Tag</label>
            <input type="text" class="form-control" placeholder="Enter tag" equired name="tag">
        </div>
        <div class="form-group text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
    @endslot
    @endcomponent
</div>
@endsection