@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-end">
        <div class="col-12">
            <div class="card">

                <div class="card-body">
                    <div class="card-title mb-4">
                        <div class="d-flex justify-content-between">
                        <div>{{$fop->tag}}</div>
                            <div class="row justify-content-center " style="height:20px">
                                <a href="{{route('fop.index')}}" class="btn mr-1 btn-success">All</a>
                                <a href="{{route('fop.edit',$fop->id)}}"
                                    class="btn mr-1 btn-primary">Edit</a>
                                <form action="{{route('fop.destroy',['id'=>$fop->id])}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn mr-1 btn-danger">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="basicInfo-tab" data-toggle="tab" href="#basicInfo"
                                        role="tab" aria-controls="basicInfo" aria-selected="true">Practices ({{$fop->practices->count()}})</a>
                                </li>
                              
                            </ul>
                            <div class="tab-content ml-1" id="myTabContent">
                                <div class="tab-pane fade show active" id="basicInfo" role="tabpanel"
                                    aria-labelledby="basicInfo-tab">
                                    @foreach($fop->practices as $practice)
                                    <div class="row">
                                        <div class="col-sm-12 ">
                                            <a href="{{route('practices.show',$practice->id)}}" class="text-capitalize">
                                                Name: {{$practice->name}} <br> Email: {{$practice->email}}</a>
                                        </div>
                                        
                                    </div>
                                    <hr />
                                    @endforeach
                                </div>
                              
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>
@endsection