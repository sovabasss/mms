<div class="row justify-content-center">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
            <h5 class="card-title pt-2">{{$type}}</h5>
                <a class="btn-danger btn " href="{{route($route)}}">New Record</a>
            </div>
            <div class="card-body">
                {{$table}}
            </div>
        </div>
    </div>
</div>
<div class="row justify-content-center p-5">{{$pagination}}</div>