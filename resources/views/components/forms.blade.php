  <div class="row justify-content-center">
      @if ($errors->any())
      <div class="col-sm-12">
      <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      </div>
      @endif
      <div class="col-sm-8">
          <div class="card">
              <div class="card-header">
              <div class="card-title">{{$type}}</div>
              </div>
              <div class="card-body">
                {{$form}}
              </div>
          </div>
      </div>
      
  </div>