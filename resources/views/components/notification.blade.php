<div class="row">
    <div class="col-sm-12 text-center">
        @if (\Session::has('success'))
        <div class="alert alert-success ">
            <p>{{ \Session::get('success') }}</p>
        </div>
        @endif
    </div>
</div>