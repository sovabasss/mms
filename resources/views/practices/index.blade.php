@extends('layouts.app')
@section('content')
<div class="container">
    @component('components.notification') @endcomponent
    @component('components.table',['route'=>'practices.create','type'=>'Practices view'])
    @slot('table')
    @if(sizeof($practices)>0)
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Logo</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Website</th>
                <th scope="col">Tags</th>
                <th scope="col">Edit</th>
            </tr>
        </thead>
        <tbody>

            @foreach($practices as $practice)
            <tr class="">
                <th scope="row">{{$practice->id}}</th>
                <td><img class="logo" src="{{$practice->avatar}}" /></td>
                <td><a class="text-capitalize" href="{{route('practices.show',$practice->id)}}">{{$practice->name}}</a>
                </td>
                <td>{{$practice->email}}</td>

                <td><a href="{{$practice->website}}" target="_blank">Link</a></td>
                <td><span class="text-success"> {{$practice->fields}}</span></td>
                <td><a href="{{route('practices.edit',['id'=>$practice->id])}}" class="btn btn-sm btn-primary">Edit</a>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>
    @else
    <div class="col-md-12 alert alert-danger text-center">No records</div>
    @endif
    @endslot
    @slot('pagination')
    {{ $practices->links() }}
    @endslot
    @endcomponent
</div>
@endsection