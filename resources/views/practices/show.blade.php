@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-end">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title mb-4">
                        <div class="d-flex justify-content-start">
                            <div class="image-container">
                                <img src="{{$practice->avatar}}" id="imgProfile" style="width: 150px; height: 150px"
                                    class="img-thumbnail" />
                            </div>
                            <div class="userData ml-3">
                                <h2 class="d-block" style="font-size: 1.5rem; font-weight: bold">
                                    <a href="{{route('practices.show',$practice->id)}}">{{$practice->name}}</a></h2>
                                <h6 class="d-block">
                                    <span class="font-weight-bold">Email:</span>
                                    {{$practice->email}}</h6>
                                <h6 class="d-block"> <span class="font-weight-bold">Tags:</span> {{$practice->fields}}
                                </h6>
                                <h6 class="d-block"> <span class="font-weight-bold">External link:</span> <a
                                        href="{{$practice->website}}" target="_blank">Link</a></h6>
                            </div>
                            <div class="ml-auto">
                                <input type="button" class="btn btn-primary d-none" id="btnDiscard"
                                    value="Discard Changes" />
                            </div>
                            <div class="row justify-content-center " style="height:20px">
                                <a href="{{route('practices.index')}}" class="btn mr-1 btn-success">All</a>
                                <a href="{{route('practices.edit',$practice->id)}}"
                                    class="btn mr-1 btn-primary">Edit</a>
                                <form action="{{route('practices.destroy',['id'=>$practice->id])}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn mr-1 btn-danger">Delete</button>
                                </form>

                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="basicInfo-tab" data-toggle="tab" href="#basicInfo"
                                        role="tab" aria-controls="basicInfo" aria-selected="true">Employees
                                        ({{$practice->employees->count()}})</a>
                                </li>

                            </ul>
                            <div class="tab-content ml-1" id="myTabContent">
                                <div class="tab-pane fade show active" id="basicInfo" role="tabpanel"
                                    aria-labelledby="basicInfo-tab">
                                    @foreach($practice->employees as $employe)
                                    <div class="row">
                                        <div class="col-sm-12 ">
                                            <a href="{{route('employees.show',$employe->id)}}" class="text-capitalize">
                                                {{$employe->first_name}}
                                                {{$employe->last_name}}
                                            </a>

                                        </div>
                                        <div class="col-md-12 col-6">
                                            {{$employe->email}} - {{$employe->phone}}
                                        </div>
                                    </div>
                                    <hr />
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection